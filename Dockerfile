FROM python:3.7-slim

ENV TZ UTC

RUN apt-get -y update && apt-get install -y zip gcc musl-dev libffi-dev libssl-dev tzdata

COPY ./ /tmp/cli
RUN cd /tmp/cli && pip install ./ -U --no-cache && rm -rf /tmp/cli
