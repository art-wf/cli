# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

__version__ = '0.0.2'

setup(
    name='art_cli',
    version=__version__,
    url='https://gitlab.com/art9/cli',
    py_modules=['art_cli'],
    packages=find_packages(),
    install_requires=[
        'click==7.0',
        'jira==2.0.0.',
        'networkx==2.4',
        'python-gitlab==1.13.0',
        'pyyaml==5.1.2',
        'yaml-injection>=0.0.4',
        'slack-notifications==0.0.6',
        'markdownify==0.4.1',
    ],
    entry_points={
        'console_scripts': [
            'art-cli=art_cli.__main__:call',
        ],
    },
)
