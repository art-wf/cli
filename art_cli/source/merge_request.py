# -*- coding: utf-8 -*-

from art_cli import dsl


class MergeRequestSource(dsl.SourceBlock):
    wip = dsl.SourceBlock('wip', type=bool, default=None)
    target_branch = dsl.SourceBlock('target_branch', type=str, default=None)

    class Merge(dsl.SourceBlock):
        no_merge_into = dsl.SourceBlock('no_merge_into', type=list, default=[])

        class IfError(dsl.SourceBlock):
            issue_status = dsl.SourceBlock('issue_status', type=[str, list], default=None)
            issue_comment = dsl.SourceBlock('issue_comment', type=[bool, str], default=None)

        if_error = IfError('if_error', type=dict, default={})

    merge = Merge('merge', type=[bool, dict], default={})

    class Labels(dsl.SourceBlock):
        include = dsl.SourceBlock('include', type=list, default=[])
        exclude = dsl.SourceBlock('exclude', type=list, default=[])

    labels = Labels('labels', type=[dict, list], default={})

    class CheckApproveOrCommit(dsl.SourceBlock):

        class IfError(dsl.SourceBlock):
            issue_status = dsl.SourceBlock('issue_status', type=[str, list], default=None)
            issue_comment = dsl.SourceBlock('issue_comment', type=[bool, list], default=None)

        if_error = IfError('if_error', type=dict, default={})

    check_approve = CheckApproveOrCommit('check_approve', type=[bool, dict], default=None)
    check_commit = CheckApproveOrCommit('check_commit', type=[bool, dict], default=None)
