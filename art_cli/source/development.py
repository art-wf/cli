# -*- coding: utf-8 -*-

from art_cli import dsl
from art_cli.source import issue
from art_cli.source import notifications
from art_cli.source import merge_request


class DevelopmentSource(dsl.SourceBlock):

    class DevStepSource(dsl.SourceBlock):

        issue = issue.IssueSource('issue', type=dict, default={})
        merge_request = merge_request.MergeRequestSource('merge_request', type=dict, default={})
        slack_notify = notifications.SlackNotifySource('slack_notify', type=dict, default={})
        allow_failure = dsl.SourceBlock('allow_failure', type=bool, default=False)

    in_progress = DevStepSource('in_progress', type=dict, default={})
    review = DevStepSource('review', type=dict, default={})
    ready_to_test = DevStepSource('ready_to_test', type=dict, default={})
    ready_to_release = DevStepSource('ready_to_release', type=dict, default={})
