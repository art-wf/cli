# -*- coding: utf-8 -*-

from art_cli import dsl
from art_cli.source.release import ReleaseSource
from art_cli.source.settings import SettingsSource
from art_cli.source.development import DevelopmentSource


class Source(dsl.Source):

    release = ReleaseSource('release', type=dict, default={})
    settings = SettingsSource('settings', type=dict, default={})
    development = DevelopmentSource('development', type=dict, default={})
