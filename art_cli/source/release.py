# -*- coding: utf-8 -*-

from art_cli import dsl
from art_cli.source import issue
from art_cli.source import repository
from art_cli.source import notifications
from art_cli.source import merge_request


class ReleaseSource(dsl.SourceBlock):

    class CreateUpdateDoClose(dsl.SourceBlock):
        issue = issue.ReleaseIssueSource('issue', type=dict, default={})
        merge_request = merge_request.MergeRequestSource('merge_request', type=dict, default={})
        slack_notify = notifications.SlackNotifySource('slack_notify', type=dict, default={})
        repository = repository.RepositoryFrame('repository', type=dict, default={})
        semver = dsl.SourceBlock('semver', type=[bool, dict], default=False)

    create = CreateUpdateDoClose('create', type=dict, default={})
    update = CreateUpdateDoClose('update', type=dict, default={})
    do = CreateUpdateDoClose('do', type=dict, default={})
    close = CreateUpdateDoClose('close', type=dict, default={})
