# -*- coding: utf-8 -*-

from art_cli import dsl


class IssueSource(dsl.SourceBlock):

    class IssueStatus(dsl.SourceBlock):
        def validate(self):
            if isinstance(self.value, list):
                for item in self.value:
                    if not isinstance(item, (str, dict)):
                        self._errors.append('Issue status can be str or dict only')

    status = IssueStatus('status', type=[str, list], default=None)
    responsible_qa = dsl.SourceBlock('responsible_qa', type=str, default=None)

    class Only(dsl.SourceBlock):
        projects = dsl.SourceBlock('projects', type=list, default=[])
        in_statuses = dsl.SourceBlock('in_statuses', type=list, default=[])

    only_ = Only('only', type=dict, default={})

    class Except(dsl.SourceBlock):
        projects = dsl.SourceBlock('projects', type=list, default=[])
        in_statuses = dsl.SourceBlock('in_statuses', type=list, default=[])

    except_ = Except('except', type=dict, default={})

    class Labels(dsl.SourceBlock):
        include = dsl.SourceBlock('include', type=list, default=[])
        exclude = dsl.SourceBlock('exclude', type=list, default=[])

    labels = Labels('labels', type=[dict, list], default={})
    resolution = dsl.SourceBlock('resolution', type=str, default=None)
    components = dsl.SourceBlock('components', type=list, default=None)


class ReleaseIssueSource(IssueSource):
    project = dsl.SourceBlock('project', type=str, default=None)
    issue_type = dsl.SourceBlock('type', type=str, default=None)

    class FixVersion(dsl.SourceBlock):
        name = dsl.SourceBlock('name', type=str, default=None)
        issues = IssueSource('issues', type=dict, default={})
        released = dsl.SourceBlock('released', type=bool, default=None)

    fix_version = FixVersion('fix_version', type=dict, default={})
