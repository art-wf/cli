# -*- coding: utf-8 -*-

from art_cli import dsl


class SlackNotifySource(dsl.SourceBlock):

    class ChannelsAndMentions(dsl.SourceBlock):

        def validate(self):
            for item in self.value:
                if not isinstance(item, (str, dict)):
                    self._errors.append('Slack channel/mention can be str or dict only')

    message = dsl.SourceBlock('message', type=str, default=None)
    channels = ChannelsAndMentions('channels', type=list, default=[])
    mentions = ChannelsAndMentions('mentions', type=list, default=[])
    issue_info = dsl.SourceBlock('issue_info', type=[list, bool], default=True)
    merge_request_info = dsl.SourceBlock('merge_request_info', type=bool, default=False)
    disable_mentions_in = dsl.SourceBlock('disable_mentions_in', type=list, default=[])
    mention_for_mr_assignees = dsl.SourceBlock('mention_for_mr_assignees', type=bool, default=False)

    class OnlyExcept(dsl.SourceBlock):
        jira_projects = dsl.SourceBlock('jira_projects', type=list, default=[])

    only_ = OnlyExcept('only', type=dict, default={})
    except_ = OnlyExcept('except', type=dict, default={})
