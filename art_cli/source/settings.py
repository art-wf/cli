# -*- coding: utf-8 -*-

from art_cli import dsl


class SettingsSource(dsl.SourceBlock):

    class Daemon(dsl.SourceBlock):
        url = dsl.SourceBlock('url', type=str, default=None)
        token = dsl.SourceBlock('token', type=str, default=None)
        enable_ssl = dsl.SourceBlock('enable_ssl', type=bool, default=False)

    daemon = Daemon('daemon', type=dict, default={})

    class Integrations(dsl.SourceBlock):

        class Gitlab2Jira(dsl.SourceBlock):
            merge_request_links = dsl.SourceBlock('merge_request_links', type=bool, default=False)
            commit_links = dsl.SourceBlock('commit_links', type=bool, default=False)
            jira_issue_transition_id = dsl.SourceBlock('jira_issue_transition_id', type=str, default=None)
            jira_username = dsl.SourceBlock('jira_username', type=str, default=None)
            jira_password = dsl.SourceBlock('jira_password', type=str, default=None)

        gitlab2jira = Gitlab2Jira('gitlab2jira', type=dict, default={})

        class Jira2Gitlab(dsl.SourceBlock):
            issue_info = dsl.SourceBlock('issue_info', type=[bool, list], default=False)

        jira2gitlab = Jira2Gitlab('jira2gitlab', type=dict, default={})

    integrations = Integrations('integrations', type=dict, default={})

    class Repository(dsl.SourceBlock):

        class ProtectedBranches(dsl.SourceBlock):
            def validate(self):
                for item in self.value:
                    if not isinstance(item, dict):
                        self._errors.append('Protected branch can be dict only')
                        continue

                    if 'name' not in item:
                        self._errors.append('"name" is required key for protected branch')

                    for allowed_to in ('allowed_to_push', 'allowed_to_merge'):
                        if not isinstance(item.get(allowed_to, {}), dict):
                            self._errors.append(f'"{allowed_to}" can be dict only')

                        for key in ('roles', 'users', 'groups'):
                            if not isinstance(item.get(allowed_to, {}).get(key, []), list):
                                self._errors.append(f'"{key}" can be list only')

        protected_branches = ProtectedBranches('protected_branches', type=list, default=[])

        class ProtectedTags(dsl.SourceBlock):
            def validate(self):
                for item in self.value:
                    if not isinstance(item, dict):
                        self._errors.append('Protected tag can be dict only')
                        continue

                    if 'name' not in item:
                        self._errors.append('"name" is required key for protected tag')

                    if 'allowed_to_create' not in item:
                        self._errors.append('"allowed_to_create" is required for protected tags')

                    if not isinstance(item['allowed_to_create'], str):
                        self._errors.append('"allowed_to_create" can be string type only')

        protected_tags = ProtectedTags('protected_tags', type=list, default=[])

        class MergeRequestApprovals(dsl.SourceBlock):
            required = dsl.SourceBlock('required', type=[int, bool], default=None)
            overriding_approvers_per_merge_request = dsl.SourceBlock(
                'overriding_approvers_per_merge_request', type=bool, default=True,
            )
            reset_approvals_on_push = dsl.SourceBlock('reset_approvals_on_push', type=bool, default=True)
            prevent_merge_requests_author_approval = dsl.SourceBlock(
                'merge_requests_author_approval', type=bool, default=True,
            )
            merge_requests_disable_committers_approval = dsl.SourceBlock(
                'merge_requests_disable_committers_approval', type=bool, default=False,
            )
            require_password_to_approve = dsl.SourceBlock('require_password_to_approve', type=bool, default=False)

        merge_request_approvals = MergeRequestApprovals('merge_request_approvals', type=dict, default={})

    repository = Repository('repository', type=dict, default={})
