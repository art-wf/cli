# -*- coding: utf-8 -*-

from art_cli import dsl


class RepositoryFrame(dsl.SourceBlock):

    delete_merged_branches = dsl.SourceBlock('delete_merged_branches', type=bool, default=True)
