# -*- coding: utf-8 -*-

from datetime import datetime

import click

from art_cli import dsl
from art_cli import utils
from art_cli.frames import issue
from art_cli.frames import repository
from art_cli.frames import integrations
from art_cli.frames import merge_request
from art_cli.frames import notifications


def call(ctx,
         source_path: str, *,
         slack_config: dict = None,
         pre_init_frames: list = None,
         post_init_frames: list = None,
         pre_issue_frames: list = None,
         post_issue_frames: list = None,
         pre_merge_request_frames: list = None,
         post_merge_request_frames: list = None,
         pre_version_frames: list = None,
         final_actions: list = None,
         post_version_frames: list = None):

    pre_init_instructions = dsl.combine_frames(*pre_init_frames or [])
    init_instructions = dsl.combine_frames(
        issue.InitIssueFrame(source_path),
        merge_request.InitMergeRequestsFrame(source_path),
    )
    post_init_instructions = dsl.combine_frames(*post_init_frames or [])

    pre_issue_instructions = dsl.combine_frames(*pre_issue_frames or [])
    issue_instructions = dsl.combine_frames(
        issue.IssueTransitionStatusFrame(source_path),
        issue.IssueLabelsFrame(source_path),
        issue.IssueResponsibleQaFrame(source_path),
        issue.ReInitIssueFrame(source_path),
    )
    post_issue_instructions = dsl.combine_frames(*post_issue_frames or [])

    pre_merge_request_instructions = dsl.combine_frames(*pre_merge_request_frames or [])
    merge_request_instructions = dsl.combine_frames(
        merge_request.MergeRequestActualizeWipStatusFrame(source_path),
        merge_request.MergeRequestLabelsFrame(source_path),
    )
    post_merge_request_instructions = dsl.combine_frames(*post_merge_request_frames or [])

    pre_version_instructions = dsl.combine_frames(*pre_version_frames or [])
    version_instructions = dsl.combine_frames(
        issue.CreateOrUpdateReleaseIssueVersionFrame(source_path),
        issue.FixVersionIssuesTransitionStatusFrame(source_path),
        issue.FixVersionIssuesLabelsFrame(source_path),
        issue.IssueVersionsReleasedStatusFrame(source_path),
    )
    post_version_instructions = dsl.combine_frames(*post_version_frames or [])

    integration_instructions = dsl.combine_frames(
        integrations.GitlabToJiraIntegrationFrame('settings.integrations'),
        integrations.JiraToGitlabIntegrationFrame('settings.integrations'),
    )

    notification_instructions = dsl.combine_frames(
        notifications.SlackNotifyFrame(source_path, **slack_config or {}),
        notifications.SlackWorkLogNotifyFrame(source_path, **slack_config or {}),
    )

    ctx.obj.interpreter.add_instruction(pre_init_instructions)
    ctx.obj.interpreter.add_instruction(init_instructions)
    ctx.obj.interpreter.add_instruction(post_init_instructions)

    ctx.obj.interpreter.add_instruction(pre_issue_instructions)
    ctx.obj.interpreter.add_instruction(issue_instructions)
    ctx.obj.interpreter.add_instruction(post_issue_instructions)

    ctx.obj.interpreter.add_instruction(pre_merge_request_instructions)
    ctx.obj.interpreter.add_instruction(merge_request_instructions)
    ctx.obj.interpreter.add_instruction(post_merge_request_instructions)

    ctx.obj.interpreter.add_instruction(pre_version_instructions)
    ctx.obj.interpreter.add_instruction(version_instructions)
    ctx.obj.interpreter.add_instruction(post_version_instructions)

    ctx.obj.interpreter.add_instruction(integration_instructions)

    final_actions_instruction = dsl.combine_frames(*final_actions or [])
    ctx.obj.interpreter.add_instruction(final_actions_instruction)

    ctx.obj.interpreter.add_instruction(notification_instructions)

    ctx.obj.interpreter.interpret()


@click.group('release')
def release():
    pass


@release.command('create')
@click.pass_context
def create(ctx):
    source_path = 'release.create'

    now = datetime.now()
    date_now = now.strftime('%Y-%m-%d')
    time_now = now.strftime('%Y-%m-%d %H:%M')

    call(
        ctx,
        source_path,
        slack_config={
            'message': 'Release is created',
        },
        pre_init_frames=[
            merge_request.CheckDiffsAfterMergeRequestsFrame(source_path),
            issue.CreateIssueFrame(source_path, **{
                'summary': f'Release {ctx.obj.gitlab_project.path} at {date_now}',
                'description': f'Release created automatically with ART at {time_now}',
            }),
            repository.CreateSourceBranchFrame(
                source_path, prefix='release/',
            ),
        ],
        pre_merge_request_frames=[
            merge_request.CreateMergeRequestFrame(source_path),
        ],
    )


@release.command('update')
@click.pass_context
def update(ctx):
    source_path = 'release.update'
    release_type = utils.get_release_type(ctx.obj.gitlab_branch, upper=True)

    call(
        ctx,
        source_path,
        slack_config={
            'messages': {
                'on_change_issue_status': f'{release_type} is updated',
            },
        },
        pre_issue_frames=[
            issue.IssueComponentsFrame(source_path),
        ],
        pre_merge_request_frames=[
            merge_request.CreateMergeRequestFrame(source_path),
        ],
    )


@release.command('do')
@click.pass_context
def do(ctx):
    source_path = 'release.do'
    release_type = utils.get_release_type(ctx.obj.gitlab_branch)

    call(
        ctx,
        source_path,
        slack_config={
            'message': f'Deploy {release_type}',
        },
        post_init_frames=[
            merge_request.CheckMergeRequestsHasApproveFrame(source_path)
        ] if not ctx.obj.gitlab_branch.name.startswith('hotfix/') else None,
        pre_issue_frames=[
            issue.IssueComponentsFrame(source_path),
        ],
        post_merge_request_frames=[
            merge_request.MergeRequestMergeActionFrame(source_path),
        ],
        post_version_frames=[
            repository.CreateReleaseFrame(source_path),
        ],
    )


@release.command('close')
@click.pass_context
def close(ctx):
    source_path = 'release.close'
    release_type = utils.get_release_type(ctx.obj.gitlab_branch, upper=True)

    call(
        ctx,
        source_path,
        slack_config={
            'message': f'{release_type} is closed',
        },
        pre_issue_frames=[
            issue.IssueComponentsFrame(source_path),
        ],
        post_merge_request_frames=[
            merge_request.MergeRequestMergeActionFrame(source_path),
        ],
        post_init_frames=[
            merge_request.CheckMergeRequestsHasApproveFrame(source_path),
        ],
        final_actions=[
            repository.DeleteMergedBranchesFrame(source_path),
        ],
    )
