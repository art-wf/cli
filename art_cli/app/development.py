# -*- coding: utf-8 -*-

import logging
from operator import attrgetter

import click

from art_cli import dsl
from art_cli.frames import issue
from art_cli.frames import integrations
from art_cli.frames import merge_request
from art_cli.frames import notifications


logger = logging.getLogger(__name__)


def call(ctx, source_path: str, *, slack_config: dict = None):
    source = attrgetter(source_path)(ctx.obj.source)

    init_instructions = dsl.combine_frames(
        issue.InitIssueFrame(source_path),
        merge_request.InitMergeRequestsFrame(source_path),
        merge_request.CheckMergeRequestsHasApproveFrame(source_path),
    )
    issue_instructions = dsl.combine_frames(
        issue.IssueTransitionStatusFrame(source_path),
        issue.IssueResponsibleQaFrame(source_path),
        issue.IssueLabelsFrame(source_path),
        issue.IssueComponentsFrame(source_path),
        issue.ReInitIssueFrame(source_path),
    )
    merge_request_instructions = dsl.combine_frames(
        merge_request.CreateMergeRequestFrame(
            source_path,
            target_branch_rules=[
                {
                    'source_branch_prefix': 'hotfix/',
                    'target_branch': ctx.obj.gitlab_default_branch.name,
                },
            ],
        ),
        merge_request.MergeRequestActualizeWipStatusFrame(source_path),
        merge_request.MergeRequestLabelsFrame(source_path),
        merge_request.CheckLastCommitFromMergeRequestsFrame(source_path),
        merge_request.MergeRequestMergeActionFrame(source_path),
    )
    integration_instructions = dsl.combine_frames(
        integrations.GitlabToJiraIntegrationFrame('settings.integrations'),
        integrations.JiraToGitlabIntegrationFrame('settings.integrations'),
    )
    notification_instructions = dsl.combine_frames(
        notifications.SlackNotifyFrame(source_path, **slack_config or {}),
        notifications.SlackWorkLogNotifyFrame(source_path, **slack_config or {}),
    )

    ctx.obj.interpreter.add_instruction(init_instructions)
    ctx.obj.interpreter.add_instruction(issue_instructions)
    ctx.obj.interpreter.add_instruction(merge_request_instructions)
    ctx.obj.interpreter.add_instruction(integration_instructions)
    ctx.obj.interpreter.add_instruction(notification_instructions)

    ctx.obj.interpreter.interpret(allow_failure=source.allow_failure.value)


@click.group('development')
def development():
    pass


@development.command('in-progress')
@click.pass_context
def in_progress(ctx):
    if ctx.obj.user in ('gitlab-web', 'gitlab-schedule'):
        return logger.info(f'Skip "in-progress" because user is "{ctx.obj.user}"')

    source_path = 'development.in_progress'

    call(ctx, source_path, slack_config={
        'messages': {
            'on_change_issue_status': 'Task in progress',
        },
    })


@development.command('review')
@click.pass_context
def review(ctx):
    source_path = 'development.review'

    call(ctx, source_path, slack_config={
        'messages': {
            'on_change_issue_status': 'Task review',
            'on_current_issue_status': 'Task review (update)',
        },
    })


@development.command('ready-to-test')
@click.pass_context
def ready_to_test(ctx):
    source_path = 'development.ready_to_test'

    call(ctx, source_path, slack_config={
        'messages': {
            'on_change_issue_status': 'Task ready to test',
            'on_current_issue_status': 'Task ready to test (update)',
        },
    })


@development.command('ready-to-release')
@click.pass_context
def ready_to_release(ctx):
    source_path = 'development.ready_to_release'

    call(ctx, source_path, slack_config={
        'messages': {
            'on_change_issue_status': 'Task ready to release',
            'on_current_issue_status': 'Task ready to release (update)',
        },
    })
