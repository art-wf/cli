# -*- coding: utf-8 -*-

import click

from art_cli import dsl
from art_cli.app import app
from art_cli.frames import daemon
from art_cli.frames import repository
from art_cli.frames import integrations


@app.command('setup')
@click.pass_context
def setup(ctx):
    repository_instructions = dsl.combine_frames(
        repository.GitlabProtectedBranchesFrame('settings.repository'),
        repository.GitlabProtectedTagsFrame('settings.repository'),
        repository.GitlabMergeRequestApprovalsFrame('settings.repository'),
    )

    integration_instructions = dsl.combine_frames(
        integrations.ConfigureGitlabToJiraIntegrationFrame('settings.integrations'),
        daemon.SetupDaemonFrame('settings'),
    )

    ctx.obj.interpreter.add_instruction(repository_instructions)
    ctx.obj.interpreter.add_instruction(integration_instructions)

    ctx.obj.interpreter.interpret()
