# -*- coding: utf-8 -*-

import os
import logging

import click

from art_cli import dsl
from art_cli import clients
from art_cli import constants
from art_cli.source import Source


logger = logging.getLogger(__name__)


class AppContextObject(dict):

    def __getattr__(self, item):
        try:
            return self[item]
        except KeyError:
            raise AttributeError(
                f'"{self.__class__.__name__}" has not attribute "{item}"',
            )

    def __setattr__(self, key, value):
        self[key] = value


class CliApplication(click.Group):

    def main(self, *args, **kwargs):
        kwargs.setdefault('prog_name', 'art-cli')
        kwargs.setdefault('obj', AppContextObject())

        return super(CliApplication, self).main(*args, **kwargs)


@click.group(cls=CliApplication)
@click.option('--user', '-u', default=os.getenv('USER', 'root'))
@click.option('--debug/--no-debug', default=False)
@click.option('--jira-url', envvar=constants.JIRA_URL_ENV, required=True)
@click.option('--jira-username', envvar=constants.JIRA_LOGIN_ENV, default=None)
@click.option('--jira-password', envvar=constants.JIRA_PASSWORD_ENV, default=None)
@click.option('--jira-token', envvar=constants.JIRA_TOKEN_ENV, default=None)
@click.option('--gitlab-url', envvar=constants.GITLAB_URL_ENV, required=True)
@click.option('--gitlab-username', envvar=constants.GITLAB_LOGIN_ENV, default=None)
@click.option('--gitlab-password', envvar=constants.GITLAB_PASSWORD_ENV, default=None)
@click.option('--gitlab-token', envvar=constants.GITLAB_TOKEN_ENV, default=None)
@click.option('--config-file', default=constants.CONFIG_FILE, type=click.File())
@click.option('--slack-token', envvar=constants.SLACK_TOKEN_ENV, default=None)
@click.option('--gitlab-project-path', '-p', required=True)
@click.option('--gitlab-source-branch', '-b', required=True)
@click.option('--gitlab-default-branch', '-d', required=True)
@click.option('--gitlab-commit-short-sha', '-c', required=True)
@click.pass_context
def app(ctx,
        user,
        debug,
        jira_url,
        jira_username,
        jira_password,
        jira_token,
        gitlab_url,
        gitlab_username,
        gitlab_password,
        gitlab_token,
        config_file,
        slack_token,
        gitlab_project_path,
        gitlab_source_branch,
        gitlab_default_branch,
        gitlab_commit_short_sha):
    """CLI utility for agile release train process"""
    logging.basicConfig(
        format='%(levelname)s: %(message)s', level=logging.DEBUG if debug else logging.INFO,
    )

    jira_access = clients.JiraAccess(
        login=jira_username, password=jira_password, token=jira_token,
    )
    clients.init_jira_client(jira_url, jira_access)

    gitlab_access = clients.GitlabAccess(
        login=gitlab_username, password=gitlab_password, token=gitlab_token,
    )
    clients.init_gitlab_client(gitlab_url, gitlab_access)

    slack_access = clients.SlackAccess(
        token=slack_token, check_requirements=False,
    )
    clients.init_slack_client(slack_access)

    ctx.obj.user = user
    ctx.obj.source = Source(config_file, gitlab_token)

    ctx.obj.gitlab_project = clients.gitlab.projects.get(gitlab_project_path)
    ctx.obj.gitlab_branch = ctx.obj.gitlab_project.branches.get(gitlab_source_branch)
    ctx.obj.gitlab_commit = ctx.obj.gitlab_project.commits.get(gitlab_commit_short_sha)
    ctx.obj.gitlab_default_branch = ctx.obj.gitlab_project.branches.get(gitlab_default_branch)

    ctx.obj.interpreter = dsl.Interpreter(ctx.obj.source, state={
        'gitlab_project': ctx.obj.gitlab_project,
        'gitlab_branch': ctx.obj.gitlab_branch,
        'gitlab_commit': ctx.obj.gitlab_commit,
        'gitlab_default_branch': ctx.obj.gitlab_default_branch,
    })


def main(*args, **kwargs):
    from art_cli.app.setup import setup
    from art_cli.app.release import release
    from art_cli.app.development import development

    sources = [
        setup,
        release,
        development,
    ]

    for source in sources:
        app.add_command(source)

    app(*args, **kwargs)
