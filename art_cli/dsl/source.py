# -*- coding: utf-8 -*-

import os
import abc
from copy import copy
from typing import IO

from yaml import load
from yaml_injection import InjectionLoader

from art_cli.dsl import exceptions
from art_cli.utils import HTTPBearerAuth


class BaseSource(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def src(self) -> dict:
        pass


class SourceBlock(BaseSource):

    def __init__(self, name: str, *, type=None, default=None, required=False):
        self.__name = name
        self.__src = None
        self.__types = []
        self.__default = default
        self.__required = required

        self.__was_validated = False

        self._errors = []

        if type is not None:
            if isinstance(type, (list, tuple)):
                self.__types.extend(type)
            else:
                self.__types.append(type)

    def __repr__(self):
        return f'{self.__class__.__name__}: {self.__name}'

    def __get__(self, instance, owner):
        if instance is None:
            return

        if self.__required and self.__name not in instance.src:
            raise exceptions.SourceError(f'Block "{self.__name}" is required for "{repr(self)}"')

        self.__src = instance.src.get(self.__name, object)

        if self.__types and self.__src is not object \
                and type(self.value) not in self.__types:
            raise exceptions.SourceError(
                f'Incorrect type of block "{self.__name}", can be in "{self.__types}", only',
            )

        if not self.__was_validated:
            self.validate()
            self.__was_validated = True

        if self._errors:
            raise exceptions.SourceError(*self._errors)

        return self

    @property
    def name(self) -> str:
        return self.__name

    @property
    def src(self) -> dict:
        return {} if self.__src == object else self.__src

    @property
    def value(self):
        return copy(self.__default) if self.__src == object else self.__src

    def validate(self):
        pass


class Source(BaseSource):

    def __init__(self, fp: IO, gitlab_token):
        def env_constructor(loader, node):
            value = loader.construct_scalar(node)
            return os.getenv(value)

        InjectionLoader.add_constructor('!env', env_constructor)

        self.__src = load(fp, InjectionLoader.authorized_loader(HTTPBearerAuth(gitlab_token)))

    @property
    def src(self) -> dict:
        return self.__src
