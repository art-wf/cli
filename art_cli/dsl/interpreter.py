# -*- coding: utf-8 -*-

import sys
import abc
import logging
import operator
from types import FunctionType
from typing import Union, Generator

from art_cli.dsl import exceptions
from art_cli.dsl.source import Source


logger = logging.getLogger(__name__)


class FrameResult:

    def __init__(self, frame, message: str, *, result=None, error=None):
        self.frame = frame
        self.message = message
        self.result = result
        self.error = error

    @property
    def has_result(self):
        return self.result is not None

    @property
    def has_error(self):
        return self.error is not None


class FrameState:

    def __init__(self, frame, current_state):
        self.__frame = frame
        self.__current_state = current_state

    @property
    def source(self):
        return self.__frame.source

    def require(self, name: str):
        if name not in self.__frame.__state__.get('requirements', []):
            raise exceptions.InterpreterError(
                f'State "{name}" is not required in "{self.__frame.__class__.__name__}.__state__"',
            )

        return self.__current_state[name]

    def declare(self, name: str, value):
        if name not in self.__frame.__state__.get('declarations', []):
            raise exceptions.InterpreterError(
                f'State "{name}" is not declared in "{self.__frame.__class__.__name__}.__state__"',
            )

        self.__current_state[name] = value

    def update(self, name: str, value):
        if name not in self.__frame.__state__.get('requirements', []):
            raise exceptions.InterpreterError(
                f'State "{name}" is not required in "{self.__frame.__class__.__name__}.__state__"',
            )

        if name not in self.__current_state:
            raise exceptions.InterpreterError(
                f'State "{name}" can not be updated, because not declare earlier',
            )

        self.__current_state[name] = value

    def validate(self):
        if not isinstance(self.__frame.__state__, dict):
            raise exceptions.InterpreterError(
                f'"{self.__frame.__class__.__name__}.__state__" attribute can be "dict" type only',
            )

        for key in self.__frame.__state__:
            if key not in ('declarations', 'requirements'):
                raise exceptions.InterpreterError(
                    f'Unexpected key "{key}" in "{self.__class__.__name__}.__state__"',
                )

    def check_requirements(self, declarations: list, extend_declarations=False):
        for require in self.__frame.__state__.get('require', []):
            if require not in declarations and require not in self.__current_state:
                raise exceptions.InterpreterError(
                    f'State "{require}" is required for frame "{repr(self.__frame)}", but not declared earlier',
                )

        if extend_declarations:
            declarations.extend(self.__frame.__state__.get('declarations', []))


class Frame(metaclass=abc.ABCMeta):

    __state__ = {
        'declarations': [],
        'requirements': [],
    }

    __state_class__ = FrameState

    def __init__(self, source_path: str, **config):
        self.__source_path = source_path
        self.__config = config
        self.__state = {}
        self.__source = None

        def execute_wrapper(f):
            def wrapper():
                try:
                    for result in f():
                        if isinstance(result, FrameResult):
                            yield result
                        elif isinstance(result, str):
                            yield FrameResult(self, result)
                        else:
                            yield FrameResult(self, str(result), result=result)
                except Exception as error:
                    yield FrameResult(
                        self, f'Fatal error on frame "{repr(self)}"', error=error,
                    )
            return wrapper

        self.execute = execute_wrapper(self.execute)

        self.state.validate()

    def __repr__(self):
        return f'{self.__class__.__name__}: {self.__source_path}'

    @property
    def source(self):
        return self.__source

    @property
    def config(self):
        return self.__config

    @property
    def state(self) -> FrameState:
        return self.__state_class__(self, self.__state)

    def mount(self, interpreter):
        self.__state = interpreter.state

        if self.__source_path:
            self.__source = operator.attrgetter(self.__source_path)(interpreter.source)
        else:
            self.__source = interpreter.source

        self.state.check_requirements(interpreter.declarations, extend_declarations=True)

    @abc.abstractmethod
    def execute(self) -> Generator:
        pass


def combine_frames(*frames: Frame) -> FunctionType:
    def wrapper():
        return list(frames)

    return wrapper


class Interpreter:

    def __init__(self, source: Source, state: dict = None):
        self.__source = source
        self.__state = state or {}

        self.__frames = []
        self.__declarations = []

    @property
    def state(self):
        return self.__state

    @property
    def source(self):
        return self.__source

    @property
    def declarations(self):
        return self.__declarations

    def add_instruction(self, instruction: Union[Frame, FunctionType]):
        if isinstance(instruction, FunctionType):
            for frame in instruction():
                frame.mount(self)
                self.__frames.append(frame)
        elif isinstance(instruction, Frame):
            instruction.mount(self)
            self.__frames.append(instruction)

    def go(self) -> Generator:
        for frame in self.__frames:
            yield from frame.execute()

    def interpret(self, *, allow_failure=False):
        for frame_result in self.go():
            logger.debug(f'Frame "{repr(frame_result.frame)}" was executed')

            if frame_result.has_result:
                logger.debug(f'RESULT: {frame_result.result}')

            if frame_result.has_error:
                sys.stdout.flush()

                logger.debug(f'ERROR: error on frame "{repr(frame_result.frame)}"')

                if allow_failure:
                    logger.error(frame_result.error, exc_info=True)
                    break

                raise frame_result.error
            else:
                sys.stdout.write(f'OK: {frame_result.message}\n')
