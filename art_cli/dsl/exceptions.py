# -*- coding: utf-8 -*-


class BaseDSLError(Exception):
    pass


class SourceError(BaseDSLError):
    pass


class InterpreterError(BaseDSLError):
    pass
