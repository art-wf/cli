# -*- coding: utf-8 -*-

import os
import traceback
from contextlib import contextmanager

from art_cli import app
from art_cli import constants
from art_cli.clients import slack


ERROR_CHANNEL = os.getenv('ART_SLACK_ERROR_CHANNEL')

CI_PROJECT_PATH = os.getenv('CI_PROJECT_PATH')

CI_JOB_URL = os.getenv('CI_JOB_URL')
CI_JOB_NAME = os.getenv('CI_JOB_NAME')
CI_JOB_USER_NAME = os.getenv('GITLAB_USER_NAME')


def call():
    with send_error_to_slack():
        app.main()


@contextmanager
def send_error_to_slack():
    try:
        yield
    except Exception as error:
        tb = traceback.format_exc()

        if not ERROR_CHANNEL or not slack.ACCESS_TOKEN:
            raise error

        try:
            attachment = slack.Attachment(
                title=f'{CI_PROJECT_PATH} job executing error <{CI_JOB_URL}|{CI_JOB_NAME}>',
                text=f'```{tb}```',
                footer=f'Launched by *{CI_JOB_USER_NAME}*',
                color='red',
            )
            slack.send_notify(ERROR_CHANNEL, username=constants.SLACK_BOT_NAME, attachments=[attachment])
        except Exception as slack_error:
            raise slack_error from error

        raise error


if __name__ == '__main__':
    call()
