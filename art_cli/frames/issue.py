# -*- coding: utf-8 -*-

import os
import re
import logging
from datetime import date

from jira.exceptions import JIRAError
from networkx import DiGraph, shortest_path, path_graph, NetworkXNoPath

from art_cli import dsl
from art_cli import utils
from art_cli import clients


logger = logging.getLogger(__name__)


class BaseEditIssueFrame(dsl.Frame):

    @property
    def except_projects(self):
        return self.source.issue.except_.projects.value

    @property
    def except_in_statuses(self):
        return self.source.issue.except_.in_statuses.value

    @property
    def only_projects(self):
        return self.source.issue.only_.projects.value

    @property
    def only_in_statuses(self):
        return self.source.issue.only_.in_statuses.value

    def need_except(self, issue=None, *, transition_to_status=None):
        issue = issue or self.state.require('issue')
        jira_project, _ = issue.key.split('-', 1)
        current_status = issue.fields.status.name

        if transition_to_status and current_status == transition_to_status:
            logger.info(f'Issue already in status "{current_status}", stop operation')
            return True

        if self.only_in_statuses and current_status not in self.only_in_statuses:
            logger.info(f'Status "{current_status}" is not in the list of allowed statuses, stop operation')
            return True

        if current_status in self.except_in_statuses:
            logger.info(f'Status "{current_status}" is excluded, stop operation')
            return True

        if self.only_projects and jira_project not in self.only_projects:
            logger.info(f'The project "{jira_project}" is not in the list of allowed projects, stop operation')
            return True

        if jira_project in self.except_projects:
            logger.info(f'Project "{jira_project}" in excepted, stop operation')
            return True

        return False

    def execute(self):
        yield


class BaseEditReleaseIssueFrame(BaseEditIssueFrame):

    @property
    def except_projects(self):
        return self.source.issue.fix_version.issues.except_.projects.value

    @property
    def except_in_statuses(self):
        return self.source.issue.fix_version.issues.except_.in_statuses.value

    @property
    def only_projects(self):
        return self.source.issue.fix_version.issues.only_.projects.value

    @property
    def only_in_statuses(self):
        return self.source.issue.fix_version.issues.only_.in_statuses.value

    @property
    def issue(self):
        return self.state.require('issue')

    @property
    def version_issues(self):
        for fix_version in self.issue.fields.fixVersions:
            version_issues = utils.jira_issues_search(
                clients.jira, f'fixVersion = "{fix_version.name}" AND key != "{self.issue.key}"',
            )

            for version_issue in version_issues:
                yield version_issue

    def need_except(self, issue, **kwargs):
        return super(BaseEditReleaseIssueFrame, self).need_except(issue, **kwargs)


class InitIssueFrame(dsl.Frame):

    __state__ = {
        'declarations': ['issue', 'rendered_issue'],
        'requirements': ['gitlab_branch'],
    }

    def execute(self):
        gitlab_branch = self.state.require('gitlab_branch')

        logger.info(f'Start operation: init issue from "{gitlab_branch.name}" branch')

        issue_id = utils.get_issue_id(gitlab_branch.name)

        if not issue_id:
            raise LookupError(f'Issue id not found in branch name "{gitlab_branch.name}')

        issue = clients.jira.issue(issue_id)
        rendered_issue = clients.jira.issue(issue_id, expand='renderedFields')

        self.state.declare('issue', issue)
        self.state.declare('rendered_issue', rendered_issue)

        yield f'Issue "{issue.permalink()}" was inited successfully'


class IssueTransitionStatusFrame(BaseEditIssueFrame):

    __state__ = {
        'requirements': ['issue'],
        'declarations': ['issue_status_was_changed'],
    }

    @staticmethod
    @utils.retry(5, delay=1, exc_cls=JIRAError)
    def transition_issue_to_status(issue, to_status):
        # Нужно обновить issue для того чтобы знать текущий статус на данный момент
        issue = clients.jira.issue(issue.key)

        if issue.fields.status.name == to_status:
            return

        logger.info(
            f'Try to transition status for issue "${issue.key}" from "${issue.fields.status.name}" to "${to_status}"',
        )

        with clients.jira_session() as session:
            browse_issue = session.get(f'{clients.jira.client_info()}/browse/{issue.key}')
            wf_name = re.search(r'workflowName=(.+?)&', browse_issue.text).group(1)

            wf_info = session.get(
                f'{clients.jira.client_info()}/rest/workflowDesigner/latest/workflows?name={wf_name}'
            ).json()

        statuses = {s['id']: s['name'] for s in wf_info['layout']['statuses']}
        transitions = DiGraph(
            (statuses[t['sourceId']], statuses[t['targetId']], {'id': t['actionId']})
            for t in wf_info['layout']['transitions']
        )

        try:
            for u, v in path_graph(shortest_path(transitions, issue.fields.status.name, to_status)).edges():
                clients.jira.transition_issue(issue.key, transitions[u][v]['id'])
        except NetworkXNoPath as path_error:  # TODO fix for all statuses in all statuses
            try:
                clients.jira.transition_issue(issue.key, to_status)
            except JIRAError as jira_error:
                raise jira_error from path_error

    @staticmethod
    def get_status_by_rules(issue, rules):
        if isinstance(rules, list):
            to_status = None
            jira_project, _ = issue.key.split('-', 1)

            for rule in rules:
                if isinstance(rule, dict) and jira_project in rule:
                    if isinstance(rule[jira_project], dict) and issue.fields.issuetype.name in rule[jira_project]:
                        to_status = rule[jira_project][issue.fields.issuetype.name]
                    elif isinstance(rule[jira_project], str):
                        to_status = rule[jira_project]
                elif isinstance(rule, dict) and issue.fields.issuetype.name in rule:
                    to_status = rule[issue.fields.issuetype.name]
                elif isinstance(rule, str):
                    to_status = rule

            return to_status

        return rules

    def execute(self):
        self.state.declare('issue_status_was_changed', False)

        issue = self.state.require('issue')
        current_status = issue.fields.status.name

        to_status = self.get_status_by_rules(issue, self.source.issue.status.value)

        logger.info('Start operation: issue transition status')

        if not to_status:
            return logger.info('Stop operation: issue transition status. Status not found.')

        if self.need_except(transition_to_status=to_status):
            return logger.info('Stop operation: issue transition status. Because only/except rules.')

        self.transition_issue_to_status(issue, to_status)

        self.state.declare('issue_status_was_changed', True)

        yield f'Transition issue status from "{current_status}" to "{to_status}" was completed successfully'


class IssueResponsibleQaFrame(BaseEditIssueFrame):

    __state__ = {
        'requirements': ['issue'],
    }

    def execute(self):
        responsible_qa = self.source.issue.responsible_qa.value

        if not responsible_qa:
            return logger.info('Skip operation: set responsible qa field to issue')

        issue = self.state.require('issue')

        logger.info('Start operation: set responsible qa field to issue')

        if self.need_except():
            return logger.info('Stop operation: set responsible qa field to issue. Because only/except rules.')

        field_name_to_id = {f['name']: f['id'] for f in clients.jira.fields()}

        if 'Responsible QA' not in field_name_to_id:
            return logger.info('Stop operation: set responsible qa field to issue. Field does not exists.')

        if getattr(issue.fields, field_name_to_id['Responsible QA']):
            return logger.info('Stop operation: set responsible qa field to issue. Already set.')

        issue.update(
            fields={
                field_name_to_id['Responsible QA']: {
                    'name': responsible_qa,
                }
            }
        )

        yield f'Responsible QA "{responsible_qa}" was set successfully to issue'


class IssueLabelsFrame(BaseEditIssueFrame):

    __state__ = {
        'requirements': ['issue'],
    }

    @staticmethod
    def get_label_value_for_gitlab_user_login():
        return os.getenv('GITLAB_USER_LOGIN')

    @staticmethod
    def get_label_value_for_gitlab_user_name():
        return os.getenv('GITLAB_USER_NAME')

    @staticmethod
    def get_label_value_for_gitlab_project():
        return os.getenv('CI_PROJECT_NAME')

    @staticmethod
    def get_label_value_for_gitlab_branch():
        project = os.getenv('CI_PROJECT_NAME')
        branch = os.getenv('CI_COMMIT_REF_NAME')

        if not project or not branch:
            return

        return f'{project}/{branch}'

    def get_label_variable_value(self, name):
        variables_map = {
            '$gitlab_branch': self.get_label_value_for_gitlab_branch,
            '$gitlab_project': self.get_label_value_for_gitlab_project,
            '$gitlab_user_name': self.get_label_value_for_gitlab_user_name,
            '$gitlab_user_login': self.get_label_value_for_gitlab_user_login,
        }

        label_getter = variables_map.get(name)

        if not label_getter:
            raise NameError(f'Unknown label variable "{name}"')

        return label_getter()

    def actualize_label(self, issue, label, source_name):
        if label.startswith('$'):
            label_value = self.get_label_variable_value(label)

            if not label_value:
                return logger.info(f'Value for label variable "{label}" was not found')
        else:
            label_value = label

        label_value = label_value.replace(' ', '_')

        if label_value in issue.fields.labels and source_name == 'exclude':
            logger.info(
                f'Delete label "{label_value}" from issue "{issue.key}"'
            )
            issue.fields.labels.remove(label_value)
        elif label_value not in issue.fields.labels and source_name == 'include':
            logger.info(
                f'Append label "{label_value}" to issue {issue.key}'
            )
            issue.fields.labels.append(label_value)

    def execute(self):
        if isinstance(self.source.issue.labels.value, list):
            include = self.source.issue.labels.value
            exclude = []
        else:
            include = self.source.issue.labels.include.value
            exclude = self.source.issue.labels.exclude.value

        if not include and not exclude:
            return logger.info('Skip operation: actualize labels for issue')

        issue = self.state.require('issue')

        logger.info('Start operation: actualize labels for issue')

        if self.need_except():
            return logger.info('Stop operation: actualize labels for issue. Because only/except rules.')

        for label in include:
            self.actualize_label(issue, label, 'include')

        for label in exclude:
            self.actualize_label(issue, label, 'exclude')

        issue.update(fields={'labels': issue.fields.labels})

        yield 'Labels for issue was actualized successfully'


class CreateIssueFrame(dsl.Frame):

    __state__ = {
        'declarations': ['issue', 'rendered_issue'],
    }

    def execute(self):
        project = self.source.issue.project.value
        issue_type = self.source.issue.issue_type.value
        components = self.source.issue.components.value

        if not project and not issue_type:
            return logger.info('Skip operation: create issue')

        logger.info('Start operation: create issue')

        summary = self.config.get('summary', 'New issue')
        description = self.config.get('description', '')

        fields = {
            'project': {'key': project},
            'summary': summary,
            'description': description,
            'issuetype': {'name': issue_type},
        }

        if components:
            fields['components'] = [{'name': c} for c in components]

        issue = clients.jira.create_issue(fields=fields)
        rendered_issue = clients.jira.issue(issue.key, expand='renderedFields')

        self.state.declare('issue', issue)
        self.state.declare('rendered_issue', rendered_issue)

        yield f'Issue "{issue.permalink()}" was create successfully'


class FixVersionIssuesTransitionStatusFrame(BaseEditReleaseIssueFrame):

    __state__ = {
        'requirements': ['issue'],
    }

    def transition_version_issue(self, issue):
        current_status = issue.fields.status.name
        to_status = IssueTransitionStatusFrame.get_status_by_rules(
            issue, self.source.issue.fix_version.issues.status.value,
        )
        log_message = f'transition issue "{issue.key}" from status ' \
                      f'"{current_status}" to status "{to_status}"'

        logger.info(f'Start operation: {log_message}')

        if self.need_except(issue, transition_to_status=to_status):
            return logger.info(f'Stop operation: {log_message}. Because except rules.')

        IssueTransitionStatusFrame.transition_issue_to_status(issue, to_status)

        yield f'Transition version issue status from "{current_status}" to "{to_status}" was completed successfully'

    def execute(self):
        if not self.source.issue.fix_version.issues.status.value or not self.issue.fields.fixVersions:
            return logger.info('Skip operation: transition status for fix version issues')

        logger.info(f'Start operation: transition status for version issues of "{self.issue.key}""')

        for version_issue in self.version_issues:
            yield from self.transition_version_issue(version_issue)

        yield 'Transition status for fix version issues was completed successfully'


class FixVersionIssuesLabelsFrame(BaseEditReleaseIssueFrame, IssueLabelsFrame):

    __state__ = {
        'requirements': ['issue'],
    }

    def apply_labels_to_version_issue(self, issue, include, exclude):
        logger.info(f'Start operation: actualize labels for issue "{issue.key}"')

        if self.need_except(issue):
            return logger.info(f'Stop operation: actualize labels for issue "{issue.key}". Because except rules.')

        for label in include:
            self.actualize_label(issue, label, 'include')

        for label in exclude:
            self.actualize_label(issue, label, 'exclude')

        issue.update(fields={'labels': issue.fields.labels})

        yield f'Labels for issue {issue.key} was actualized successfully'

    def execute(self):
        if not self.issue.fields.fixVersions:
            return logger.info('Skip operation: set labels for version issues')

        if isinstance(self.source.issue.fix_version.issues.labels.value, list):
            include = self.source.issue.fix_version.issues.labels.value
            exclude = []
        else:
            include = self.source.issue.fix_version.issues.labels.include.value
            exclude = self.source.issue.fix_version.issues.labels.exclude.value

        if not include and not exclude:
            return logger.info('Skip operation: set labels for version issues')

        logger.info(f'Start operation: actualize labels for version issues of "{self.issue.key}"')

        for version_issue in self.version_issues:
            yield from self.apply_labels_to_version_issue(version_issue, include, exclude)

        yield 'Labels for fix version issues was actualized successfully'


class IssueVersionsReleasedStatusFrame(dsl.Frame):

    __state__ = {
        'requirements': ['issue'],
    }

    def execute(self):
        released = self.source.issue.fix_version.released.value

        if released is None:
            return logger.info('Skip operation: actualize released status for fix versions')

        issue = self.state.require('issue')

        data = {
            'released': released,
            'releaseDate': date.today().strftime('%Y-%m-%d') if released else None,
        }

        logger.info('Start operation: actualize released status for fix versions')

        for fix_version in issue.fields.fixVersions:
            fix_version.update(**data)

            yield f'Released status for version "{fix_version}" was updated successfully'


class ReInitIssueFrame(dsl.Frame):

    __state__ = {
        'requirements': ['issue', 'rendered_issue'],
    }

    def execute(self):
        issue = self.state.require('issue')

        issue = clients.jira.issue(issue.key)
        rendered_issue = clients.jira.issue(issue.key, expand='renderedFields')

        self.state.update('issue', issue)
        self.state.update('rendered_issue', rendered_issue)

        yield f'Issue "{issue.key}" was re-inited successfully'


class CreateOrUpdateReleaseIssueVersionFrame(dsl.Frame):

    __state__ = {
        'requirements': [
            'issue',
            'merge_requests',
            'gitlab_project',
            'gitlab_branch',
        ],
    }

    def __init__(self, *args, **kwargs):
        super(CreateOrUpdateReleaseIssueVersionFrame, self).__init__(*args, **kwargs)

        self.__version_name = None
        self.__version_issues = None
        self.__version_projects = None
        self.__project_to_version = None

    @property
    def version_name(self) -> str:
        if self.__version_name is None:
            issue = self.state.require('issue')
            gitlab_project = self.state.require('gitlab_project')

            self.__version_name = f'{gitlab_project.path.lower()}/{issue.key.lower()}'

        return self.__version_name

    @property
    def version_issues(self) -> list:
        if self.__version_issues is None:
            issue_ids = []
            commit_ids = []

            self.__version_issues = [self.state.require('issue')]

            gitlab_project = self.state.require('gitlab_project')
            gitlab_branch = self.state.require('gitlab_branch')
            merge_requests = self.state.require('merge_requests')

            for merge_request in merge_requests:
                compare_branches = gitlab_project.repository_compare(
                    merge_request.target_branch, gitlab_branch.name,
                )

                for compare_commit in compare_branches['commits']:
                    commit_short_id = compare_commit['short_id']

                    if commit_short_id not in commit_ids:
                        gitlab_commit = gitlab_project.commits.get(commit_short_id)
                        issue_id = utils.get_issue_id(gitlab_commit.message)

                        if issue_id and issue_id not in issue_ids:
                            issue_ids.append(issue_id)
                            self.__version_issues.append(clients.jira.issue(issue_id))

                        commit_ids.append(commit_short_id)

        return self.__version_issues

    @property
    def version_projects(self) -> list:
        if self.__version_projects is None:
            self.__version_projects = []

            for issue in self.version_issues:
                project, _ = issue.key.split('-', 1)

                if project not in self.__version_projects:
                    self.__version_projects.append(project)

        return self.__version_projects

    @property
    def project_to_version(self) -> dict:
        if self.__project_to_version is None:
            self.__project_to_version = {}

            for project in self.version_projects:
                if project not in self.__project_to_version:
                    project_versions = clients.jira.project_versions(project)

                    for project_version in project_versions:
                        if project_version.name == self.version_name:
                            self.__project_to_version[project] = project_version
                            break

        return self.__project_to_version

    def execute(self):
        merge_requests = self.state.require('merge_requests')

        if not merge_requests:
            return logger.info('Skip operation: create version')

        logger.info('Start operation: create version')

        release_issue = self.state.require('issue')

        start_date = date.today().strftime('%Y-%m-%d')
        description = f'Version created from ART. Release issue: {release_issue.permalink()}'

        for project in self.version_projects:
            logger.info(f'Create version "{self.version_name}" for project "{project}" if not exist')

            if project not in self.project_to_version:
                self.__project_to_version[project] = clients.jira.create_version(
                    self.version_name, project,
                    description=description, startDate=start_date,
                )
                yield f'Version "{self.version_name}" for project "{project}" was created successfully'
            else:
                logger.info(f'Version "{self.version_name}" already exists in project "{project}"')

        for issue in self.version_issues:
            current_version_names = [v.name for v in issue.fields.fixVersions]

            if self.version_name not in current_version_names:
                updated_versions = [{'name': n} for n in current_version_names]
                updated_versions.append({'name': self.version_name})

                issue.update(fields={'fixVersions': updated_versions})

                yield f'Version "{self.version_name}" was added to issue "{issue.key}" successfully'
            else:
                logger.info(f'Version "{self.version_name}" already added to issue "{issue.key}"')

        yield f'Version "{self.version_name}" was created successfully'


class IssueComponentsFrame(BaseEditIssueFrame):

    __state__ = {
        'requirements': ['issue'],
    }

    def execute(self):
        if not self.source.issue.components.value:
            return logger.info('Skip operation: actualize components for issue')

        logger.info('Start operation: actualize components for issue')

        if self.need_except():
            return logger.info('Stop operation: actualize components for issue. Because only/except rules.')

        issue = self.state.require('issue')
        components = [c.name for c in issue.fields.components]

        for component in self.source.issue.components.value:
            if component not in components:
                components.append(component)

        issue.update(fields={'components': [{'name': c} for c in components]})

        yield 'Issue components was actualized successfully'
