# -*- coding: utf-8 -*-

import os
import logging

from gitlab.exceptions import GitlabGetError

from art_cli import dsl
from art_cli import utils
from art_cli import clients


logger = logging.getLogger(__name__)


class CreateSourceBranchFrame(dsl.Frame):

    __state__ = {
        'requirements': ['issue', 'gitlab_project', 'gitlab_branch'],
    }

    @staticmethod
    def create_branch_if_not_exist(gitlab_project, ref, branch):
        try:
            gitlab_project.branches.get(branch)
        except GitlabGetError:
            return gitlab_project.branches.create({'branch': branch, 'ref': ref})

        return None

    def execute(self):
        issue = self.state.require('issue')
        gitlab_project = self.state.require('gitlab_project')
        gitlab_branch = self.state.require('gitlab_branch')

        prefix = self.config.get('prefix', '')
        new_source_branch = prefix + issue.key

        logger.info(f'Start operation: create git branch "{new_source_branch}"')

        new_branch = self.create_branch_if_not_exist(
            gitlab_project, gitlab_branch.name, new_source_branch,
        )

        if new_branch:
            self.state.update('gitlab_branch', new_branch)
            yield f'Branch "{new_source_branch}" from "{gitlab_branch.name}" was created successfully'
        else:
            logger.info(f'Stop operation: create git branch. Branch "{new_source_branch}" already exists.')


class GitlabProtectedBranchesFrame(dsl.Frame):

    __state__ = {
        'requirements': ['gitlab_project', 'gitlab_branch'],
    }

    GITLAB_ACCESS_LEVELS = {
        'No one': {'access_level': 0},
        'Admins': {'access_level': 60},
        'Developers': {'access_level': 30},
        'Maintainers': {'access_level': 40},
    }

    def allowed_to(self, key, rule):
        allowed_to = []

        for role in rule.get(key, {}).get('roles', []):
            if role in self.GITLAB_ACCESS_LEVELS:
                allowed_to.append(self.GITLAB_ACCESS_LEVELS[role])
            else:
                logger.info(f'Role "{role}" not found in "{list(self.GITLAB_ACCESS_LEVELS.keys())}"')

        for user in rule.get(key, {}).get('users', []):
            gitlab_users = clients.gitlab.users.list(username=user)

            if gitlab_users:
                allowed_to.append({'user_id': gitlab_users[0].id})
            else:
                logger.info(f'User "{user}" not found')

        for group in rule.get(key, {}).get('groups', []):
            gitlab_groups = clients.gitlab.groups.list(search=group)

            if gitlab_groups:
                allowed_to.append({'group_id': gitlab_groups[0].id})
            else:
                logger.info(f'Group "{group}" not found')

        return allowed_to

    def execute(self):
        gitlab_project = self.state.require('gitlab_project')
        gitlab_branch = self.state.require('gitlab_branch')

        if not self.source.protected_branches.value:
            return logger.info('Skip operation: configure protected branches')

        logger.info('Start operation: configure protected branches')

        for branch in gitlab_project.protectedbranches.list():
            branch.delete()

        for rule in self.source.protected_branches.value:
            branch_name = rule['name']

            logger.info(f'Try to protect branch "{branch_name}"')

            if CreateSourceBranchFrame.create_branch_if_not_exist(
                gitlab_project, gitlab_branch.name, branch_name,
            ):
                logger.info(f'Branch "{branch_name}" was created successfully')

            gitlab_project.protectedbranches.create({
                'name': branch_name,
                'allowed_to_push': self.allowed_to('allowed_to_push', rule),
                'allowed_to_merge': self.allowed_to('allowed_to_merge', rule),
            })

            yield f'Branch "{branch_name}" was protected successfully'


class GitlabProtectedTagsFrame(dsl.Frame):

    __state__ = {
        'requirements': ['gitlab_project', 'gitlab_branch'],
    }

    GITLAB_ACCESS_LEVELS = {
        'Admins': 60,
        'Developers': 30,
        'Maintainers': 40,
    }

    def execute(self):
        gitlab_project = self.state.require('gitlab_project')

        if not self.source.protected_tags.value:
            return logger.info('Skip operation: configure protected tags')

        logger.info('Start operation: configure protected tags')

        for tag in gitlab_project.protectedtags.list():
            tag.delete()

        for rule in self.source.protected_tags.value:
            tag_name = rule['name']
            access_level = self.GITLAB_ACCESS_LEVELS.get(rule['allowed_to_create'])

            if not access_level:
                logger.info(f'Incorrect access level "{access_level}" for tag "{tag_name}"')
                continue

            logger.info(f'Try to protect tag "{tag_name}"')

            gitlab_project.protectedtags.create({
                'name': tag_name,
                'create_access_level': access_level,
            })

            yield f'Tag "{tag_name}" was protected successfully'


class GitlabMergeRequestApprovalsFrame(dsl.Frame):

    __state__ = {
        'requirements': ['gitlab_project'],
    }

    def execute(self):
        if self.source.merge_request_approvals.required.value is None:
            return logger.info('Skip operation: configure merge request approvals')

        logger.info('Start operation: configure merge request approvals')

        gitlab_project = self.state.require('gitlab_project')

        approvals = gitlab_project.approvals.get()

        if isinstance(self.source.merge_request_approvals.required.value, bool):
            approvals.approvals_before_merge = 1
        elif isinstance(self.source.merge_request_approvals.required.value, int):
            approvals.approvals_before_merge = self.source.merge_request_approvals.required.value

        approvals.disable_overriding_approvers_per_merge_request = \
            not self.source.merge_request_approvals.overriding_approvers_per_merge_request.value

        approvals.reset_approvals_on_push = \
            self.source.merge_request_approvals.reset_approvals_on_push.value

        approvals.merge_requests_author_approval = \
            not self.source.merge_request_approvals.prevent_merge_requests_author_approval.value

        approvals.merge_requests_disable_committers_approval = \
            self.source.merge_request_approvals.merge_requests_disable_committers_approval.value

        approvals.require_password_to_approve = \
            self.source.merge_request_approvals.require_password_to_approve.value

        approvals.save()

        yield 'Gitlab merge request approvals was configured successfully'


class CreateReleaseFrame(dsl.Frame):

    __state__ = {
        'requirements': ['issue', 'gitlab_project', 'gitlab_branch'],
    }

    def __init__(self, *args, **kwargs):
        super(CreateReleaseFrame, self).__init__(*args, **kwargs)

        self.__tag_name = None

    @property
    def tag_name(self):
        if self.__tag_name is None:
            semver = self.source.semver.value
            tag_from_env = os.getenv('ART_VERSION')

            if tag_from_env:
                self.__tag_name = tag_from_env
            elif semver:
                prefix = semver['prefix'] if isinstance(semver, dict) and semver.get('prefix') else None

                gitlab_project = self.state.require('gitlab_project')
                tags = gitlab_project.tags.list()

                if prefix:
                    tags = list(filter(lambda t: t.name.startswith(prefix), tags))

                if tags:
                    latest_version = tags[0].name

                    try:
                        first, second, thrid = latest_version.split('.')
                    except ValueError as error:
                        raise ValueError(
                            f'Latest tag "{latest_version}" is not semver format. '
                            f'Use "ART_VERSION" variable for resolve it.'
                        ) from error

                    thrid = str(int(thrid) + 1)
                    self.__tag_name = f'{first}.{second}.{thrid}'
                else:
                    raise LookupError(
                        'No one tag not found in project. '
                        'Use "ART_VERSION" variable for create first version.',
                    )
            else:
                issue = self.state.require('issue')
                self.__tag_name = issue.key

        return self.__tag_name

    def create_release_notes(self):
        release_issue = self.state.require('issue')

        notes = [
            '### Version\n'
            f'* **Tag:** {self.tag_name}',
            f'* **Release issue:** [{release_issue.key}]({release_issue.permalink()})',
        ]
        start_issues_flag = False
        issue_ids = [release_issue.key]

        for fix_version in release_issue.fields.fixVersions:
            for version_issue in utils.jira_issues_search(clients.jira, f'fixVersion = "{fix_version.name}"'):
                if version_issue.key in issue_ids:
                    continue

                if not start_issues_flag:
                    notes.append('### Issues')
                    start_issues_flag = True

                issue_ids.append(version_issue.key)

                notes.append(
                    f'* [{version_issue.key}]({version_issue.permalink()}): {version_issue.fields.summary}',
                )

        return '\n'.join(notes)

    def execute(self):
        logger.info('Start operation: create release')

        issue = self.state.require('issue')
        gitlab_project = self.state.require('gitlab_project')
        gitlab_branch = self.state.require('gitlab_branch')

        gitlab_project.tags.create({
            'tag_name': self.tag_name,
            'ref': gitlab_branch.name,
        })
        yield f'Tag "{self.tag_name}" was set on branch "{gitlab_branch.name}"'

        gitlab_project.releases.create({
            'tag_name': self.tag_name,
            'name': f'[{issue.key}]: {issue.fields.summary}',
            'description': self.create_release_notes(),
        })
        yield f'Release "{issue.fields.summary}" was created successfully'


class DeleteMergedBranchesFrame(dsl.Frame):

    __state__ = {
        'requirements': ['gitlab_project'],
    }

    def execute(self):
        if not self.source.repository.delete_merged_branches.value:
            return logger.info('Skip operation: delete merged branches')

        gitlab_project = self.state.require('gitlab_project')

        logger.info('Start operation: delete merged branches')

        gitlab_project.delete_merged_branches()

        yield 'Merged branches was deleted successfully'
