# -*- coding: utf-8 -*-

import os
import logging

from jira.exceptions import JIRAError
from gitlab.exceptions import GitlabError

from art_cli import dsl
from art_cli import utils
from art_cli import clients
from art_cli import constants
from art_cli.frames.issue import IssueTransitionStatusFrame


logger = logging.getLogger(__name__)


class InitMergeRequestsFrame(dsl.Frame):

    __state__ = {
        'declarations': ['merge_requests'],
        'requirements': ['gitlab_project', 'gitlab_branch'],
    }

    def execute(self):
        gitlab_project = self.state.require('gitlab_project')
        gitlab_branch = self.state.require('gitlab_branch')

        logger.info(
            f'Start operation: init merge request list '
            f'for "{gitlab_branch.name}" branch in project "{gitlab_project.path}"',
        )

        opened_mrs = gitlab_project.mergerequests.list(state='opened')

        def is_valid(mr):
            if mr.source_branch != gitlab_branch.name:
                return False

            for label in mr.labels:
                if label in constants.SEARCH_BY_LABELS:
                    return True

            return False

        merge_requests = list(filter(is_valid, opened_mrs))
        self.state.declare('merge_requests', merge_requests)

        yield f'Merge request list was inited successfully, count "{len(merge_requests)}"'


class CreateMergeRequestFrame(dsl.Frame):

    __state__ = {
        'requirements': [
            'merge_requests',
            'gitlab_project',
            'gitlab_branch',
            'gitlab_commit',
        ],
    }

    def target_branches(self, gitlab_project):
        if self.source.merge_request.value:
            ignore_source = False

            gitlab_branch = self.state.require('gitlab_branch')
            gitlab_commit = self.state.require('gitlab_commit')
            git_commands = utils.get_git_commands_from_commit(gitlab_commit.message)

            if git_commands:
                allowed_branches = [m.source_branch for m in gitlab_project.mergerequests.list(state='opened')]
                release_branches = list(filter(lambda b: b.startswith('release/'), allowed_branches))

                if 'rls' in git_commands and not release_branches:
                    raise LookupError(
                        'No one merge request with release source branch (release/*) was not found',
                    )
                elif 'rls' in git_commands and len(release_branches) > 1:
                    raise LookupError(
                        'Conflict, release branches count > 1, use [tb:<branch>] for resolve it',
                    )
                elif 'rls' in git_commands and release_branches:
                    ignore_source = True

                    yield release_branches[0]

                for command in git_commands:
                    if command.startswith('tb:'):
                        ignore_source = True
                        branch = command.replace('tb:', '', 1)

                        if branch not in allowed_branches:
                            raise LookupError(
                                f'Merge request with source branch "{branch}" does not exist',
                            )

                        yield branch

            target_branch = self.source.merge_request.target_branch.value
            target_branch_rules = self.config.get('target_branch_rules', [])

            for target_branch_rule in target_branch_rules:
                source_branch_prefix = target_branch_rule['source_branch_prefix']

                if gitlab_branch.name.startswith(source_branch_prefix):
                    target_branch = target_branch_rule['target_branch']
                    break

            if not ignore_source and target_branch:
                yield target_branch

    def execute(self):
        gitlab_project = self.state.require('gitlab_project')
        merge_requests = self.state.require('merge_requests')
        gitlab_branch = self.state.require('gitlab_branch')

        current_target_branches = [mr.target_branch for mr in merge_requests]

        for target_branch in self.target_branches(gitlab_project):
            logger.info(
                f'Start operation: create merge request "{gitlab_branch.name} -> {target_branch}"',
            )

            if target_branch in current_target_branches:
                logger.info(
                    f'Stop operation: merge request to "{target_branch}" target branch already exists',
                )
                continue

            merge_request = gitlab_project.mergerequests.create({
                'source_branch': gitlab_branch.name,
                'target_branch': target_branch,
                'title': f'{gitlab_branch.name} → {target_branch} ({gitlab_project.path_with_namespace})',
                'labels': list(constants.CREATE_WITH_LABELS)
            })
            merge_requests.append(merge_request)

            yield f'Merge request "{gitlab_branch.name} -> {target_branch}" was created successfully'


class MergeRequestActualizeWipStatusFrame(dsl.Frame):

    __state__ = {
        'requirements': ['merge_requests'],
    }

    def execute(self):
        wip = self.source.merge_request.wip.value

        if wip is None:
            return logger.info('Skip operation: actualize wip status for merge requests')

        merge_requests = self.state.require('merge_requests')

        logger.info('Start operation: actualize wip status for merge requests')

        for merge_request in merge_requests:
            logger.info(
                f'Try actualize wip status for merge request '
                f'"{merge_request.source_branch} -> {merge_request.target_branch}"',
            )

            if wip and not merge_request.title.startswith(constants.WIP_MERGE_REQUEST_PREFIX):
                merge_request.title = constants.WIP_MERGE_REQUEST_PREFIX + merge_request.title
                merge_request.save()

                yield f'Wip status was turned on successfully for merge request ' \
                      f'"{merge_request.source_branch} -> {merge_request.target_branch}"'
            elif not wip and merge_request.title.startswith(constants.WIP_MERGE_REQUEST_PREFIX):
                merge_request.title = merge_request.title.replace(constants.WIP_MERGE_REQUEST_PREFIX, '', 1)
                merge_request.save()

                yield f'Wip status was turned off successfully for merge request ' \
                      f'"{merge_request.source_branch} -> {merge_request.target_branch}"'
            else:
                yield f'Wip status is already actual for merge request ' \
                      f'"{merge_request.source_branch} -> {merge_request.target_branch}"'


class MergeRequestMergeActionFrame(dsl.Frame):

    __state__ = {
        'requirements': ['issue', 'merge_requests'],
    }

    def execute(self):
        if not self.source.merge_request.merge.value:
            return logger.info('Skip operation: action merge for merge requests')

        issue = self.state.require('issue')
        merge_requests = self.state.require('merge_requests')

        if isinstance(self.source.merge_request.merge.value, dict):
            use_if_error = True
            no_merge_into = self.source.merge_request.merge.no_merge_into.value
        else:
            use_if_error = False
            no_merge_into = []

        logger.info('Start operation: action merge for merge requests')

        for merge_request in merge_requests:
            if merge_request.target_branch not in no_merge_into:
                logger.info(
                    f'Try merge "{merge_request.source_branch}" into "{merge_request.target_branch}"',
                )

                try:
                    merge_request.merge()
                except GitlabError as gitlab_error:
                    if use_if_error:
                        issue_status = self.source.merge_request.merge.if_error.issue_status.value
                        issue_comment = self.source.merge_request.merge.if_error.issue_comment.value

                        if issue_comment is True:
                            issue_comment = f':( Can not merge [merge request|{merge_request.web_url}]. ' \
                                            f'Please, check conflicts.'

                        try:
                            if issue_status:
                                issue_status = IssueTransitionStatusFrame.get_status_by_rules(issue, issue_status)
                                IssueTransitionStatusFrame.transition_issue_to_status(issue, issue_status)

                            if issue_comment:
                                clients.jira.add_comment(issue, issue_comment)
                        except JIRAError as jira_error:
                            raise jira_error from gitlab_error

                    raise gitlab_error

                yield f'Merge request "{merge_request.source_branch} -> {merge_request.target_branch}" ' \
                      f'was merged successfully'


class MergeRequestLabelsFrame(dsl.Frame):

    __state__ = {
        'requirements': ['issue', 'merge_requests'],
    }

    @staticmethod
    def get_label_value_for_gitlab_user_login():
        return os.getenv('GITLAB_USER_LOGIN')

    @staticmethod
    def get_label_value_for_gitlab_user_name():
        return os.getenv('GITLAB_USER_NAME')

    def get_label_value_for_issue_name(self):
        issue = self.state.require('issue')
        return issue.key

    def get_label_variable_value(self, name):
        variables_map = {
            '$issue_name': self.get_label_value_for_issue_name,
            '$gitlab_user_name': self.get_label_value_for_gitlab_user_name,
            '$gitlab_user_login': self.get_label_value_for_gitlab_user_login,
        }

        label_getter = variables_map.get(name)

        if not label_getter:
            raise NameError(f'Unknown label variable "{name}"')

        return label_getter()

    def actualize_label(self, merge_request, label, source_name):
        if label.startswith('$'):
            label_value = self.get_label_variable_value(label)

            if not label_value:
                return logger.info(f'Value for label variable "{label}" was not found')
        else:
            label_value = label

        if label_value in merge_request.labels and source_name == 'exclude':
            logger.info(
                f'Delete label "{label_value}" from merge request '
                f'"{merge_request.source_branch} -> {merge_request.target_branch}"'
            )
            merge_request.labels.remove(label_value)
        elif label_value not in merge_request.labels and source_name == 'include':
            logger.info(
                f'Append label "{label_value}" to merge request '
                f'"{merge_request.source_branch} -> {merge_request.target_branch}"'
            )
            merge_request.labels.append(label_value)

    def execute(self):
        if isinstance(self.source.merge_request.labels.src, list):
            include = self.source.merge_request.labels.src
            exclude = []
        else:
            include = self.source.merge_request.labels.include.value
            exclude = self.source.merge_request.labels.exclude.value

        if not include and not exclude:
            return logger.info('Skip operation: actualize labels to merge request')

        merge_requests = self.state.require('merge_requests')

        logger.info('Start operation: actualize labels to merge request')

        for merge_request in merge_requests:
            logger.info(
                f'Try actualize labels for merge request '
                f'"{merge_request.source_branch} -> {merge_request.target_branch}"',
            )

            for label in include:
                self.actualize_label(merge_request, label, 'include')

            for label in exclude:
                self.actualize_label(merge_request, label, 'exclude')

            merge_request.save()

            yield f'Labels for merge request ' \
                  f'"{merge_request.source_branch} -> {merge_request.target_branch}" was actualized successfully'


class CheckDiffsAfterMergeRequestsFrame(dsl.Frame):

    __state__ = {
        'requirements': [
            'gitlab_project',
            'gitlab_branch',
        ],
    }

    def execute(self):
        target_branch = self.source.merge_request.target_branch.value

        gitlab_project = self.state.require('gitlab_project')
        gitlab_branch = self.state.require('gitlab_branch')

        logger.info(f'Start operation: checks diffs between "{gitlab_branch.name}" and "{target_branch}"')

        compare_branches = gitlab_project.repository_compare(target_branch, gitlab_branch.name)

        diffs_count = len(compare_branches['diffs'])

        if diffs_count == 0:
            raise LookupError(
                f"The {gitlab_branch.name} and {target_branch} branches don't have diffs.",
            )

        commits_count = len(compare_branches['commits'])

        yield f'The {gitlab_branch.name} branch has "{commits_count}" commits after the "{target_branch}"'


class CheckMergeRequestsHasApproveFrame(dsl.Frame):

    __state__ = {
        'requirements': [
            'issue',
            'merge_requests',
        ],
    }

    def execute(self):
        if not self.source.merge_request.check_approve.value:
            return logger.info('Skip operation: checking that all merge requests has approval')

        issue = self.state.require('issue')
        merge_requests = self.state.require('merge_requests')

        use_if_error = isinstance(self.source.merge_request.check_approve.value, dict)

        logger.info('Start operation: checking that all merge requests have approval')

        unapproved = [mr for mr in merge_requests if not mr.approvals.get().approved]

        if unapproved:
            mr_urls = [mr.web_url for mr in unapproved]

            try:
                raise LookupError(
                    f'Current or associated merge request(s) unapproved. Please get approve for: {mr_urls}',
                )
            except LookupError as unapproved_error:
                if use_if_error:
                    issue_status = self.source.merge_request.check_approve.if_error.issue_status.value
                    issue_comment = self.source.merge_request.check_approve.if_error.issue_comment.value

                    if issue_comment is True:
                        issue_comment = ':( Please, get approve for merge request(s):\n{}'.format(
                                        '\n'.join(f'* [{m.title}|{m.web_url}]' for m in unapproved),
                        )

                    try:
                        if issue_status:
                            issue_status = IssueTransitionStatusFrame.get_status_by_rules(issue, issue_status)
                            IssueTransitionStatusFrame.transition_issue_to_status(issue, issue_status)

                        if issue_comment:
                            clients.jira.add_comment(issue, issue_comment)
                    except JIRAError as jira_error:
                        raise jira_error from unapproved_error

                raise unapproved_error

        yield 'All merge requests has approval'


class CheckLastCommitFromMergeRequestsFrame(dsl.Frame):

    __state__ = {
        'requirements': [
            'issue',
            'merge_requests',
            'gitlab_commit',
            'gitlab_project',
        ],
    }

    @property
    def has_changes(self):
        gitlab_project = self.state.require('gitlab_project')
        merge_requests = self.state.require('merge_requests')

        for merge_request in merge_requests:
            compare_branches = gitlab_project.repository_compare(
                merge_request.source_branch, merge_request.target_branch,
            )

            diffs_count = len(compare_branches['diffs'])

            if diffs_count > 0:
                return True

        return False

    def execute(self):
        if not self.source.merge_request.check_commit.value or not self.has_changes:
            return logger.info('Skip operation: check last commit message from merge request(s)')

        logger.info('Start operation: check last commit message from merge request(s)')

        gitlab_commit = self.state.require('gitlab_commit')

        if any(map(lambda s: s in gitlab_commit.message.lower(), ('merge branch', 'revert'))) or \
                gitlab_commit.message.lower().startswith('merge'):
            return logger.info('Stop operation: check last commit message from merge request(s)')

        issue = self.state.require('issue')

        issue_id = utils.get_issue_id(gitlab_commit.message)
        use_if_error = isinstance(self.source.merge_request.check_commit.value, dict)

        try:
            if not issue_id:
                raise LookupError('Issue id is required for commit message')
            elif issue_id != issue.key:
                raise LookupError(f'Issue id from commit message "{issue_id}" is not equal "{issue.key}"')
        except LookupError as check_error:
            if use_if_error:
                issue_status = self.source.merge_request.check_commit.if_error.issue_status.value
                issue_comment = self.source.merge_request.check_commit.if_error.issue_comment.value

                try:
                    if issue_status:
                        issue_status = IssueTransitionStatusFrame.get_status_by_rules(issue, issue_status)
                        IssueTransitionStatusFrame.transition_issue_to_status(issue, issue_status)

                    if issue_comment is True:
                        issue_comment = f'(!) Non correct issue key in ' \
                                        f'[commit|{gitlab_commit.web_url}] message. Please, fix it.'

                    if issue_comment:
                        clients.jira.add_comment(issue, issue_comment)
                except JIRAError as jira_error:
                    raise jira_error from check_error

            raise check_error

        yield 'Last commit message from merge request(s) was checked successfully'
