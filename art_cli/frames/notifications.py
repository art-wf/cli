# -*- coding: utf-8 -*-

import os
import logging

from art_cli import dsl
from art_cli import clients
from art_cli import constants


logger = logging.getLogger(__name__)


def create_issue_attachment(issue, issue_info=True):
    issue_fields = (
        ('priority', clients.slack.Attachment.Field(title='Priority', value=str(issue.fields.priority))),
        ('status', clients.slack.Attachment.Field(title='Status', value=str(issue.fields.status))),
        ('assignee', clients.slack.Attachment.Field(title='Assignee', value=str(issue.fields.assignee))),
        ('reporter', clients.slack.Attachment.Field(title='Reporter', value=str(issue.fields.reporter))),
    )

    if isinstance(issue_info, list):
        issue_fields = filter(lambda f: f[0] in issue_info, issue_fields)
    elif issue_info is False:
        issue_fields = []

    return clients.slack.Attachment(
        title=f'<{issue.permalink()}|{issue.key}>: {issue.fields.summary}',
        color='blue',
        fields=[f[1] for f in issue_fields] or None,
    )


def create_merge_request_attachment(gitlab_project, merge_request, *, issue=None):
    title_first_part = f'<{merge_request.web_url}|merge request({gitlab_project.path})>: '
    title_second_part = f'{merge_request.source_branch} → {merge_request.target_branch}'

    if issue:
        title_second_part = title_second_part.replace(issue.key, f'<{issue.permalink()}|{issue.key}>', 1)

    return clients.slack.Attachment(title=title_first_part + title_second_part, color='coral')


def get_channel_id_by_user_email(user_email):
    resource = clients.slack.Resource('users.lookupByEmail', 'GET')
    response = clients.slack.call_resource(resource, raise_exc=True, params={'email': user_email})

    data = response.json()
    user = data['user']

    return user['id']


class SlackNotifyFrame(dsl.Frame):

    __state__ = {
        'requirements': [
            'issue',
            'merge_requests',
            'gitlab_project',
            'issue_status_was_changed',
        ]
    }

    def __init__(self, *args, **kwargs):
        super(SlackNotifyFrame, self).__init__(*args, **kwargs)

        self.__message = None

    @property
    def message(self):
        if self.__message is None:
            if self.source.slack_notify.message.value:
                self.__message = self.prepare_message(
                    self.source.slack_notify.message.value,
                )
            else:
                messages = self.config.get('messages', {})
                issue_status_was_changed = self.state.require('issue_status_was_changed')

                if issue_status_was_changed:
                    message = messages.get('on_change_issue_status')
                else:
                    message = messages.get('on_current_issue_status')

                if not message:
                    message = self.config.get('message', '')

                self.__message = self.prepare_message(message)

        return self.__message

    @property
    def only_projects(self):
        return self.source.slack_notify.only_.jira_projects.value

    @property
    def except_projects(self):
        return self.source.slack_notify.except_.jira_projects.value

    def need_except(self):
        issue = self.state.require('issue')
        jira_project, _ = issue.key.split('-', 1)

        if self.only_projects and jira_project not in self.only_projects:
            logger.info(f'Jira project "{jira_project}" not in only projects')
            return True

        if jira_project in self.except_projects:
            logger.info(f'Jira project "{jira_project}" in except projects')
            return True

        return False

    def get_values_from_rules(self, rules):
        issue = self.state.require('issue')
        jira_project, _ = issue.key.split('-', 1)

        for rule in rules:
            if isinstance(rule, dict) and jira_project in rule.keys():
                project_values = rule[jira_project]
                return [project_values] if isinstance(project_values, str) else project_values

        return [r for r in rules if isinstance(r, str)]

    def prepare_message(self, message):
        issue = self.state.require('issue')

        variables = (
            ('issue_key', issue.key),
            ('issue_link', issue.permalink()),
            ('issue_title', issue.fields.summary),
        )

        for v_name, v_value in variables:
            message = message.replace('{{' + v_name + '}}', v_value)

        return message

    def prepare_channels(self, channels, *, mr_assignees=None):
        prepared_channels = set()
        issue = self.state.require('issue')

        for channel in channels:
            if channel == '$issue_assignee':
                if not issue.fields.assignee:
                    logger.info('Issue assignee is empty. $issue_assignee is ignored.')
                    continue

                prepared_channels.add(
                    get_channel_id_by_user_email(issue.fields.assignee.raw['emailAddress']),
                )
            elif channel == '$responsible_qa':
                field_name_to_id = {f['name']: f['id'] for f in clients.jira.fields()}

                if 'Responsible QA' not in field_name_to_id:
                    logger.info('"Responsible QA" field does not exists in jira project. $responsible_qa is ignored.')
                    continue

                jira_user = getattr(issue.fields, field_name_to_id['Responsible QA'], None)

                if not jira_user:
                    logger.info('"Responsible QA" field is empty. $responsible_qa is ignored.')
                    continue

                prepared_channels.add(
                    get_channel_id_by_user_email(jira_user.raw['emailAddress']),
                )
            elif channel == '$issue_reporter':
                if not issue.fields.reporter:
                    logger.info('Issue reporter is empty. $issue_reporter is ignored.')
                    continue

                prepared_channels.add(
                    get_channel_id_by_user_email(issue.fields.reporter.raw['emailAddress']),
                )
            elif channel == '$mr_assignees':
                for mr_assignee in mr_assignees or []:
                    gitlab_user = clients.gitlab.users.get(username=mr_assignee)
                    gitlab_user_email = gitlab_user.public_email or getattr(gitlab_user, 'email', None)

                    if gitlab_user_email:
                        prepared_channels.add(
                            get_channel_id_by_user_email(gitlab_user_email),
                        )
                    else:
                        logger.info(
                            f'Can not lookup user in slack. Unknown email for user "{gitlab_user.name}" in gitlab.',
                        )
            else:
                prepared_channels.add(channel)

        return prepared_channels

    def message_with_mentions(self, channel, *, mentions=None):
        mentions = mentions or self.get_values_from_rules(
            self.source.slack_notify.mentions.value,
        )
        disable_mentions_in = self.source.slack_notify.disable_mentions_in.value

        if mentions and channel not in disable_mentions_in:
            message_prefix = ' '.join(f'@{m}' for m in mentions)
            return f'{message_prefix} {self.message}'

        return self.message

    def execute(self):
        if not self.source.slack_notify.channels.value:
            return logger.info('Skip operation: send slack notifications')

        logger.info('Start operation: send slack notifications')

        if not self.message:
            return logger.info('Stop operation: send slack notifications. No message.')

        if self.need_except():
            return logger.info('Stop operation: send slack notifications. Because only/except rules.')

        issue = self.state.require('issue')
        gitlab_project = self.state.require('gitlab_project')
        merge_requests = self.state.require('merge_requests')

        channels = self.get_values_from_rules(self.source.slack_notify.channels.value)

        attachments = []
        mr_assignees = set()

        if self.source.slack_notify.issue_info.value is not False:
            attachments.append(
                create_issue_attachment(
                    issue, issue_info=self.source.slack_notify.issue_info.value,
                ),
            )

        for merge_request in merge_requests:
            for assignee in merge_request.assignees:
                mr_assignees.add(assignee['username'])

            if self.source.slack_notify.merge_request_info.value:
                attachments.append(
                    create_merge_request_attachment(
                        gitlab_project, merge_request,
                        issue=issue if self.source.slack_notify.issue_info.value is False else None,
                    ),
                )

        for channel in self.prepare_channels(channels, mr_assignees=list(mr_assignees)):
            logger.info(f'Try send notification to channel "{channel}"')

            text = self.message_with_mentions(channel,
                                              mentions=list(mr_assignees)
                                              if self.source.slack_notify.mention_for_mr_assignees.value
                                              else [])
            clients.slack.send_notify(channel,
                                      text=text,
                                      raise_exc=True,
                                      attachments=attachments,
                                      username=constants.SLACK_BOT_NAME,
                                      icon_url=constants.SLACK_BOT_AVATAR)

            yield f'Slack notification to channel "{channel}" was send successfully'


class SlackWorkLogNotifyFrame(dsl.Frame):

    __state__ = {
        'requirements': [
            'issue',
            'merge_requests',
            'gitlab_project',
            'issue_status_was_changed',
        ]
    }

    LOG_CHANNEL = os.getenv('ART_SLACK_LOG_CHANNEL')

    def __init__(self, *args, **kwargs):
        super(SlackWorkLogNotifyFrame, self).__init__(*args, **kwargs)

        self.__message = None

    @property
    def message(self):
        if self.__message is None:
            messages = self.config.get('messages', {})
            issue_status_was_changed = self.state.require('issue_status_was_changed')

            if issue_status_was_changed:
                self.__message = messages.get('on_change_issue_status')
            else:
                self.__message = messages.get('on_current_issue_status')

            if not self.__message:
                self.__message = self.config.get('message', '')

        return self.__message

    def execute(self):
        if not self.LOG_CHANNEL:
            return logger.info('Skip operation: work log notify')

        if not self.message:
            return logger.info('Skip operation: work log notify. No message.')

        logger.info('Start operation: work log notify')

        issue = self.state.require('issue')
        gitlab_project = self.state.require('gitlab_project')
        merge_requests = self.state.require('merge_requests')

        attachments = [create_issue_attachment(issue)]

        for merge_request in merge_requests:
            attachments.append(
                create_merge_request_attachment(gitlab_project, merge_request),
            )

        clients.slack.send_notify(self.LOG_CHANNEL,
                                  text=self.message,
                                  raise_exc=True,
                                  attachments=attachments,
                                  username=constants.SLACK_BOT_NAME,
                                  icon_url=constants.SLACK_BOT_AVATAR)

        yield f'Slack notification to work log channel "{self.LOG_CHANNEL}" was send successfully'
