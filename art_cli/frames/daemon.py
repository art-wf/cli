# -*- coding: utf-8 -*-

import logging

from art_cli import dsl
from art_cli import constants


logger = logging.getLogger(__name__)


class SetupDaemonFrame(dsl.Frame):

    __state__ = {
        'requirements': ['gitlab_project'],
    }

    def __init__(self, *args, **kwargs):
        super(SetupDaemonFrame, self).__init__(*args, **kwargs)

        self._current_hooks = None

    @property
    def current_hooks(self):
        if self._current_hooks is None:
            gitlab_project = self.state.require('gitlab_project')

            self._current_hooks = [
                hook for hook in gitlab_project.hooks.list()
                if all(
                    map(
                        lambda s: s in hook.url,
                        constants.DAEMON_URL_SIGNS,
                    ),
                ) or constants.DAEMON_WEB_HOOK_PATH in hook.url  # TODO:
                # delete DAEMON_WEB_HOOK_PATH check, it's supporting old version
            ]

        return self._current_hooks

    @property
    def web_hook_options(self):
        url = self.source.daemon.url.value.rstrip('/')

        options = {
            'url': url + constants.DAEMON_WEB_HOOK_PATH + f'?label={constants.DAEMON_WEB_HOOK_LABEL}',
            'push_events': False,
            'merge_requests_events': True,
            'enable_ssl_verification': self.source.daemon.enable_ssl.value,
        }

        if self.source.daemon.token.value:
            options['token'] = self.source.daemon.token.value

        return options

    def delete_current_web_hooks(self):
        logger.info('Start operation: delete all art web hooks')

        for hook in self.current_hooks:
            hook.delete()

            yield f'Web hook "{hook.url}" was deleted successfully'

    def create_web_hook(self):
        logger.info('Start operation: create art web hook')

        gitlab_project = self.state.require('gitlab_project')
        hook = gitlab_project.hooks.create(self.web_hook_options)

        yield f'Web hook "{hook.url}" was created successfully'

    def update_current_web_hook(self):
        logger.info('Start operation: update current art web hook')

        hook = next(iter(self.current_hooks))

        for option, value in self.web_hook_options.items():
            setattr(hook, option, value)

        hook.save()

        yield f'Web hook "{hook.url}" was updated successfully'

    def execute(self):
        if not self.source.daemon.value:
            yield from self.delete_current_web_hooks()

            return logger.info('Skip operation: configure daemon')

        if not self.source.daemon.url.value:
            yield from self.delete_current_web_hooks()

            return logger.info('Skip operation: configure daemon. Url is required.')

        logger.info('Start operation: configure daemon')

        if len(self.current_hooks) > 1:
            yield from self.delete_current_web_hooks()
            yield from self.create_web_hook()
        elif self.current_hooks:
            yield from self.update_current_web_hook()
        else:
            yield from self.create_web_hook()
