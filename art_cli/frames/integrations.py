# -*- coding: utf-8 -*-

import logging

from jira.exceptions import JIRAError
from markdownify import markdownify as html2md

from art_cli import dsl
from art_cli import clients
from art_cli import constants


logger = logging.getLogger(__name__)


class ConfigureGitlabToJiraIntegrationFrame(dsl.Frame):

    __state__ = {
        'requirements': ['gitlab_project'],
    }

    def execute(self):
        if not self.source.gitlab2jira.value:
            return logger.info('Skip operation: configure jira service for gitlab project')

        logger.info('Start operation: configure jira service for gitlab project')

        gitlab_project = self.state.require('gitlab_project')

        service = gitlab_project.services.get('jira')

        service.active = True
        service.merge_requests_events = False
        service.url = clients.jira.client_info()

        service.commit_events = self.source.gitlab2jira.commit_links.value

        if self.source.gitlab2jira.jira_issue_transition_id.value:
            service.jira_issue_transition_id = self.source.gitlab2jira.jira_issue_transition_id.value
        else:
            service.jira_issue_transition_id = ''

        jira_access = clients.JiraAccess()

        if self.source.gitlab2jira.jira_username.value:
            service.username = self.source.gitlab2jira.jira_username.value
        elif jira_access.has_basic:
            service.username = jira_access.login

        if self.source.gitlab2jira.jira_password.value:
            service.password = self.source.gitlab2jira.jira_password.value
        elif jira_access.has_basic:
            service.password = jira_access.password

        service.save()

        yield 'Jira service for gitlab project was configured successfully'


class GitlabToJiraIntegrationFrame(dsl.Frame):

    __state__ = {
        'requirements': ['issue', 'merge_requests', 'gitlab_project'],
    }

    def is_gitlab_mr_link(self, link):
        url = getattr(link.object, 'url', '')
        gitlab_project = self.state.require('gitlab_project')

        return clients.gitlab.url in url and gitlab_project.path_with_namespace in url

    def create_jira_issue_link_to_merge_request(self, merge_request):
        logger.info(f'Ty create link to merge request {merge_request.iid}')

        issue = self.state.require('issue')
        gitlab_project = self.state.require('gitlab_project')
        title = f'MR({gitlab_project.path}): {merge_request.source_branch} → {merge_request.target_branch}'

        return clients.jira.add_simple_link(issue, {
            'title': title,
            'url': merge_request.web_url,
            'icon': {'url16x16': constants.GITLAB_ICON_URL, 'title': 'gitlab'},
        })

    def merge_request_links(self):
        merge_request_links = self.source.gitlab2jira.merge_request_links.value

        if not merge_request_links:
            return logger.info('Skip operation: add merge request links to issue')

        issue = self.state.require('issue')
        merge_requests = self.state.require('merge_requests')

        logger.info('Start operation: add merge request links to issue')

        jira_remote_links = clients.jira.remote_links(issue)
        jira_mr_links = (
            l for l in jira_remote_links
            if self.is_gitlab_mr_link(l)
        )
        urls_to_delete = [m.web_url for m in merge_requests]

        for jira_mr_link in jira_mr_links:
            if jira_mr_link.object.url in urls_to_delete:
                logger.info(f'Delete old merge request link "{jira_mr_link.object.title}"')

                try:
                    jira_mr_link.delete()
                except JIRAError as error:
                    logger.error(error, exc_info=True)

        for merge_request in merge_requests:
            self.create_jira_issue_link_to_merge_request(merge_request)
            yield f'Link to merge request "{merge_request.iid}" was created successfully'

    def execute(self):
        if not self.source.gitlab2jira.value:
            return logger.info('Skip gitlab2jira integration')

        yield from self.merge_request_links()


class JiraToGitlabIntegrationFrame(dsl.Frame):

    __state__ = {
        'requirements': ['rendered_issue', 'merge_requests'],
    }

    def issue_info(self):
        issue_info = self.source.jira2gitlab.issue_info.value

        if not issue_info:
            return logger.info('Skip operation: sync issue info to merge request')

        if isinstance(issue_info, list):
            title = 'title' in issue_info
            description = 'description' in issue_info
        else:
            title = True
            description = True

        issue = self.state.require('rendered_issue')
        merge_requests = self.state.require('merge_requests')

        logger.info('Start operation: sync issue info to merge request')

        for merge_request in merge_requests:
            logger.info(f'Try sync issue info from "{issue.key}" to "{merge_request.iid}"')

            title_prefix = constants.WIP_MERGE_REQUEST_PREFIX \
                if merge_request.title.startswith(constants.WIP_MERGE_REQUEST_PREFIX) else ''

            if title:
                merge_request.title = title_prefix + f'[{issue.key}] {issue.fields.summary}'
            if description:
                merge_request.description = html2md(issue.renderedFields.description)

            merge_request.save()

            yield f'Issue info was synced successfully from "{issue.key}" to "{merge_request.iid}"'

    def execute(self):
        if not self.source.jira2gitlab.value:
            return logger.info('Skip jira2gitlab integration')

        yield from self.issue_info()
