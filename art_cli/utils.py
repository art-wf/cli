# -*- coding: utf-8 -*-

import re
import time
from functools import wraps

from requests.auth import AuthBase


GIT_COMMANDS_REGEXP = re.compile(r'\[[a-zA-Z0-9-><=&:;/_]+\]')


def retry(retries, *, exc_cls=Exception, delay=1):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            for i in range(retries):
                try:
                    return f(*args, **kwargs)
                except (exc_cls,):
                    if (i + 1) == retries:
                        raise
                    time.sleep(delay)
        return wrapped
    return wrapper


def get_git_commands_from_commit(commit):
    return [c[1:-1] for c in GIT_COMMANDS_REGEXP.findall(commit)]


class HTTPBearerAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = 'Bearer ' + self.token
        return r


def jira_issues_search(client, *args, **kwargs):
    max_results = 50

    issues = []

    while True:
        kwargs.update(
            maxResults=max_results,
            startAt=len(issues),
        )
        batch = client.search_issues(*args, **kwargs)

        if batch:
            issues.extend(batch)
        else:
            return issues


def get_issue_id(string: str) -> str:
    return re.search(r'\w+-\d+|$', string.upper()).group()


def get_release_type(gitlab_branch, *, upper=False):
    n = 'hotfix' if gitlab_branch.name.startswith('hotfix/') else 'release'

    if upper:
        return n[0].upper() + n[1:]

    return n


class Singleton(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)

        return cls._instances[cls]
