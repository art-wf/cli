# -*- coding: utf-8 -*-

from contextlib import contextmanager

from jira import JIRA
from gitlab import Gitlab
import slack_notifications as slack

from art_cli.utils import Singleton


JIRA_CONNECTION_TIMEOUT = 180
GITLAB_CONNECTION_TIMEOUT = 180


jira: JIRA = None
gitlab: Gitlab = None


class _BaseAccess(metaclass=Singleton):

    def __init__(self, login: str = None, password: None = None, token: str = None, check_requirements=True):
        if check_requirements:
            allow_basic_auth = getattr(self, '__allow_basic_auth__')

            if allow_basic_auth and (not login or not password) and not token:
                raise ValueError(f'"login, password" or "token" is required for "{self.__class__.__name__}"')
            elif not allow_basic_auth and not token:
                raise ValueError(f'"token" is required for "{self.__class__.__name__}"')

        self.__login = login
        self.__password = password

        self.__token = token

    @property
    def login(self):
        return self.__login

    @property
    def password(self):
        return self.__password

    @property
    def token(self):
        return self.__token

    @property
    def has_basic(self):
        return bool(self.__login) and bool(self.__password)


class GitlabAccess(_BaseAccess):

    __allow_basic_auth__ = True


class JiraAccess(_BaseAccess):

    __allow_basic_auth__ = True


class SlackAccess(_BaseAccess):

    __allow_basic_auth__ = False


def init_jira_client(url: str, access: JiraAccess):
    global jira

    if access.has_basic and not access.token:
        jira = JIRA(
            options={'server': url},
            timeout=JIRA_CONNECTION_TIMEOUT,
            basic_auth=(access.login, access.password),
        )
    else:
        jira = JIRA(url, timeout=JIRA_CONNECTION_TIMEOUT)

        with jira_session() as session:
            session.headers['Authorization'] = f'Basic {access.token}'


def init_gitlab_client(url: str, access: GitlabAccess):
    global gitlab

    if access.has_basic and not access.token:
        gitlab = Gitlab(url,
                        http_username=access.login,
                        http_password=access.password,
                        timeout=GITLAB_CONNECTION_TIMEOUT)
    else:
        gitlab = Gitlab(url,
                        private_token=access.token,
                        timeout=GITLAB_CONNECTION_TIMEOUT)


def init_slack_client(access: SlackAccess):
    slack.ACCESS_TOKEN = access.token


@contextmanager
def jira_session():
    yield getattr(jira, '_session')
